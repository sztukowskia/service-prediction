import pandas as pd
import ast

def prepare_frames(filename, label, drop=[]):
    df = _read(filename)
    df.dropna(inplace=True)
    df.drop(drop, axis=1, inplace=True)

    df, y = _extract_label(df, label)
    df = recognize_types(df)

    return df, y

def recognize_types(df):
    for column in df:
        column_type = type(ast.literal_eval(str(df[column][0])))
        df[column] = df[column].astype(column_type)

    return df


def _extract_label(df, label):
    df[label] = df[label].str.replace(',', '.')
    df[label] = df[label].astype(float)
    y = df[label]
    df.drop([label], axis=1, inplace=True)

    return df, y

def _read(filename, converters=None):
    return pd.read_csv(filename, sep=";", error_bad_lines=False, converters=converters)