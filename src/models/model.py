from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from math import sqrt
from abc import ABC, abstractmethod
import numpy as np

class Model(ABC):
    def run_model(self, X_train, X_test, y_train, y_test):
        model = self.get_base_model()
        model.fit(X_train, y_train)

        test_mae = mean_absolute_error(y_test, model.predict(X_test))
        test_mape = self.mean_absolute_percentage_error(y_test, model.predict(X_test))
        #print("Errors on testing dataset")
        print("Mean absorute error = ", test_mae, " | Mean absolute percentage error = ", test_mape)

        return test_mae, test_mape

    def mean_absolute_percentage_error(self, y_true, y_pred):
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

    @abstractmethod
    def get_base_model(self):
        pass