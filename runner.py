import src.helpers.data_provider as dp
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import src.models.k_neighbours as kn
import src.models.linear_regression as lr
import src.models.decision_tree as dt
import src.models.random_forest as rf
import src.models.neural_network as nn

# Data loading
# =============================
X, y = dp.graph_dijkstra_frame()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=123)

scaler = StandardScaler()
train_scaled = scaler.fit_transform(X_train)
test_scaled = scaler.transform(X_test)
print('DIJKSTRA')
print("Opis pomiarów czasu wykonywania żądania")
print(y.describe())

# =============================
# End of data loading



# Models
# =============================

print("K-neighbours")
kn_model = kn.KNeighbours(n_neighbors=5)
kn_model.run_model(train_scaled, test_scaled, y_train, y_test)
print('-----------------------------------------------------')
print("Linear regression")
lr_model = lr.LinearRegression()
lr_model.run_model(train_scaled, test_scaled, y_train, y_test)
print('-----------------------------------------------------')
print("Decision tree")
dt_model = dt.DecisionTree()
dt_model.run_model(train_scaled, test_scaled, y_train, y_test)
print('-----------------------------------------------------')
print("Random forest")
rf_model = rf.RandomForest()
rf_model.run_model(train_scaled, test_scaled, y_train, y_test)
print('-----------------------------------------------------')
print("Neural network")
nn_model = nn.NeuralNetwork(max_iter=5000)
nn_model.run_model(train_scaled, test_scaled, y_train, y_test)

# =============================
# End of models
